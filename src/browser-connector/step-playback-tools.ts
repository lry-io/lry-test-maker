import { Action, SelectorType, TestStep } from "../common/types";
import { getVisibleSingleElementByXPathOrFail } from "./step-creation-tools";

interface StepResult {
	success: boolean,
	error?: string
}

export function playTestStep(testStep: TestStep): StepResult {
	try {
		const elem = finderFunctions[testStep.selectorType](testStep.target)
		if (testStep.action === Action.Scroll) {
			const [x, y] = testStep.data.split(",")
			elem.scrollBy(parseInt(x), parseInt(y))
		}
		else if (testStep.action === Action.Click) {
			elem.dispatchEvent(new Event('click'))
		}
		return { success: true }
	}
	catch (e) {
		return { success: false, error: (e as Error).message }
	}
}

const finderFunctions: { [index: string]: (target: string) => HTMLElement } = {
	[SelectorType.DataTestID]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@data-testid="${target}"]`),
	[SelectorType.AriaLabel]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@aria-label="${target}"]`),
	[SelectorType.ID]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@id="${target}"]`),
	[SelectorType.LabelFor]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@for="${target}"]`),
	[SelectorType.Placeholder]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@placeholder="${target}"]`),
	[SelectorType.Text]: (target) => getVisibleSingleElementByXPathOrFail(`//*[text()="${target}"]`),
	[SelectorType.Value]: (target) => getVisibleSingleElementByXPathOrFail(`//*[@value="${target}"]`),
	[SelectorType.NestedInLabel]: getNestedInLabel,
}

function getNestedInLabel(target: string) {
	return getVisibleSingleElementByXPathOrFail(`[@for="${target}"]`)
}

