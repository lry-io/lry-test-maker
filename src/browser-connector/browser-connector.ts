import { Action, TestStep } from "../common/types";
import { addTestStep, createTestStepForElement } from "./step-creation-tools";
import { playTestStep } from "./step-playback-tools";

console.log("lry-test-maker web interface loaded")

interface TestControlState {
	actionList: TestStep[]
	browserNumber: number,
	recording: boolean,
	verifying: boolean,
	playgroundMode: boolean,
	setBrowserNumber: (newNum: number) => void,
	startRecording: Function,
	stopRecording: Function,
	startVerifying: Function,
	stopVerifying: Function,
	update: Function
	playTestStep: Function
}

declare global {
	interface Window {
		lryTestMakerIsRecording: boolean
		lryTestMakerVerifying: boolean
		lryTestMaker: TestControlState
		lryTestMakerPlaygroundMode: boolean
	}
}

const state: TestControlState = {
	browserNumber: -1, // This will be overridden by the recording_browser synch call
	recording: window.lryTestMakerIsRecording,
	verifying: window.lryTestMakerVerifying,
	playgroundMode: window.lryTestMakerPlaygroundMode,
	actionList: [],
	setBrowserNumber: (newNum: number) => { state.browserNumber = newNum },
	startRecording: () => { state.recording = true },
	stopRecording: () => { state.recording = false },
	startVerifying: () => { setVerifying(true) },
	stopVerifying: () => { setVerifying(false) },
	update: () => { },
	playTestStep
}

window.lryTestMaker = state

let lastStep: TestStep

document.addEventListener("click", (e) => {
	try {
		checkAndGenerateScrollStep()
		const step = createTestStepForElement(
			(e.target as HTMLElement),
			window.lryTestMaker.verifying ? Action.Verify : Action.Click,
			"")
		if (step) {
			lastStep = step
			addTestStep(step)
		}
		if (window.lryTestMaker.verifying) {
			e.stopImmediatePropagation()
		}
	}
	catch (e) {
		console.log("Browser connector error: ", e)
	}
});

let verifyStyleAdded = false
const verifyStyleName = 'lryTestMakerVerifying'
function addVerifyStyle() {
	verifyStyleAdded = true
	const style = document.createElement('style')
	style.innerHTML = `.${verifyStyleName} { cursor: crosshair!important; }`
	document.head.appendChild(style);
}

function setVerifying(newSetting: boolean) {
	window.lryTestMaker.verifying = newSetting
	if (window.lryTestMaker.verifying) {
		addVerifyStyle()
		document.body.classList.add(verifyStyleName)
	}
	else {
		document.body.classList.remove(verifyStyleName)
	}
}

document.addEventListener("keyup", (e) => {
	if (e.code === "Digit2" && e.ctrlKey) {
		if (!window.lryTestMaker.verifying) {
			window.lryTestMaker.startVerifying()
		}
		else {
			window.lryTestMaker.stopVerifying()
		}
	}
	else {
		try {
			checkAndGenerateScrollStep()
			if (window.lryTestMaker.verifying) {

			}
			else {
				const step = createTestStepForElement((e.target as HTMLElement), Action.Write, e.key)
				if (step) {
					lastStep = step
					addTestStep(step)
				}
			}

		}
		catch (e) {
			console.log("Browser connector error: ", e)
		}
	}
});

// Scrolling causes a lot of scroll events to fire for each user scroll
// action so as a debounce strategy we store the previous scroll element
// and then when the next event happens we store the whole lot as a single
// scroll event
let lastScrolled: EventTarget | undefined
document.addEventListener("scroll", (e) => {
	console.log(e)
	// EDGE CASE: if scroll switches from one element to another,
	// record the last scroll as a step and start a new one
	if (lastScrolled !== e.target) {
		checkAndGenerateScrollStep()
	}
	lastScrolled = e.target || undefined
});

function checkAndGenerateScrollStep() {
	if (lastScrolled) {
		const step = createTestStepForElement((lastScrolled as HTMLElement), Action.Scroll, `${window.scrollX},${window.scrollY}`)
		if (step) {
			addTestStep(step)
		}
		lastScrolled = undefined
	}
}

function playgroundPrintStepsAndClear() {
	const outBox = document.getElementById("playground-output")
	if (outBox) {
		(outBox as HTMLTextAreaElement).value =
			JSON.stringify(state.actionList, null, 2)
	}
	state.actionList = []
}
