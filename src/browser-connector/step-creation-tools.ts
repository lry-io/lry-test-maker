import { Action, SelectorType, sessionStorageKey, TestStep } from "../common/types"

function getElementsByXPath(xpath: string) {
	let results = [];
	let query = document.evaluate(xpath, document,
		null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	for (let i = 0, length = query.snapshotLength; i < length; ++i) {
		results.push(query.snapshotItem(i));
	}
	return results;
}

export function getVisibleElementsByXPath(xpath: string) {
	let results = getElementsByXPath(xpath)
	return results.filter(elem => (elem as HTMLElement).offsetParent);
}

export function getVisibleSingleElementByXPathOrFail(xpath: string) {
	const results = getVisibleElementsByXPath(xpath)
	if (results.length === 1) {
		return results[0] as HTMLElement
	}
	else if (results.length > 1) {
		throw new Error("too many elements match")
	}
	else {
		throw new Error("no elements match")
	}
}

function hasUniqueText(element: HTMLElement) {
	if (element.innerText.trim()) {
		const matchingElements = getVisibleElementsByXPath(`//*[text()="${element.innerText.trim()}"]`)
		if (matchingElements.length === 1) {
			return true
		}
	}
}

function hasUniqueValue(element: HTMLInputElement) {
	if (element.value) {
		const matchingElements = getVisibleElementsByXPath(`//*[@value="${element.value}"]`)
		if (matchingElements.length === 1) {
			return true
		}
	}
}


function walkUpTree(node: HTMLElement, apply: Function): string | undefined {
	try {
		const result = apply(node)
		if (result) {
			return result
		}
		else {
			if (node.parentNode)
				return walkUpTree((node.parentNode as HTMLElement), apply)
		}
	}
	catch (e) {
		return undefined
	}
}

function getUniqueAriaLabel(element: HTMLElement) {
	const ariaLabel = walkUpTree(element, (elem: HTMLElement) => elem.getAttribute('aria-label'))
	if (ariaLabel) {
		if (document.querySelectorAll(`[aria-label="${ariaLabel}"]`).length === 1) {
			return ariaLabel
		}
		else {
			return undefined
		}
	}
}

function hasUniquePlaceholder(element: HTMLInputElement) {
	return element.placeholder && document.evaluate(`count(//*[@placeholder="${element.placeholder}"])`, document, null, XPathResult.ANY_TYPE, null).numberValue === 1
}

function getUniqueLabel(element: HTMLInputElement) {
	return element.labels?.length === 1 && element.labels[0].innerText
}

function getUniqueName(element: HTMLInputElement) {
	if (element.name && document.querySelectorAll(`*[name="${element.name}"]`).length === 1) {
		return element.name
	}
	else {
		return null
	}
}

function getDataTestId(element: HTMLElement) {
	return element.getAttribute("data-testid") || element.getAttribute("data-test-id")
}

export function createTestStepForElement(target: HTMLElement, action: Action, data: string): TestStep | undefined {
	if (!target.tagName) {
		if (action === Action.Scroll) {
			return generateTestStep(action, SelectorType.DOCUMENT, "Scroll Document To", data)
		}
		return
	}
	const tagName = target.tagName
	const dataTestId = getDataTestId(target)
	if (tagName === "BODY") {
		if (action === Action.Scroll) {
			return generateTestStep(action, SelectorType.BODY, "Scroll Body To", data)
		}
	}
	else if (tagName === "SELECT") {
		console.log("ignore, handle in option click")
	}
	else if (tagName === "OPTION") {
		// generate step for the select box parent of this option
	}
	else if (target.innerHTML.length > 100) {
		// ignore
	}
	else if (tagName === "INPUT" || tagName === "TEXTAREA") {
		const inputElement = target as HTMLInputElement
		const uniqueLabel = getUniqueLabel(inputElement)
		const uniqueName = getUniqueName(inputElement)
		if (inputElement.type.toLowerCase() === "submit" && hasUniqueValue(inputElement)) {
			return generateTestStep(action, SelectorType.Value, inputElement.value, data)
		}
		else if (hasUniquePlaceholder(inputElement)) {
			return generateTestStep(action, SelectorType.Placeholder, inputElement.placeholder, data)
		}
		else if (uniqueLabel) {
			if (inputElement.labels && inputElement.labels[0].contains(inputElement)) {
				return generateTestStep(action, SelectorType.NestedInLabel, uniqueLabel, data)
			}
			else {
				return generateTestStep(action, SelectorType.LabelFor, uniqueLabel, data)
			}
		}
		else if (dataTestId) {
			return generateTestStep(action, SelectorType.DataTestID, dataTestId, data)
		}
		else if (uniqueName) {
			return generateTestStep(action, SelectorType.Name, uniqueName, data)
		}
		else {
			return generateTestStep(action, SelectorType.CantIdentify, "ERROR: CANT IDENTIFY", data)
		}
	}
	else { // All other elements
		if (hasUniqueText(target)) {
			return generateTestStep(action, SelectorType.Text, target.innerText.trim(), data)
		}
		else if (dataTestId) {
			return generateTestStep(action, SelectorType.DataTestID, dataTestId, data)
		}
		else {
			const ariaLabel = getUniqueAriaLabel(target)
			if (ariaLabel) {
				return generateTestStep(action, SelectorType.AriaLabel, ariaLabel, data)
			}
		}
	}
}

function generateTestStep(action: Action, selectorType: SelectorType, target: string, data: string) {
	return {
		browserNumber: window.lryTestMaker.browserNumber,
		action,
		selectorType,
		target,
		data,
		time: (new Date()).getTime()
	}
}

export function addTestStep(step: TestStep) {
	const steps = getTestSteps()
	steps.push(step)
	sessionStorage.setItem(sessionStorageKey, JSON.stringify(steps))
}

export function getTestSteps() {
	const testStepsString = sessionStorage.getItem(sessionStorageKey)
	return testStepsString ? JSON.parse(testStepsString) : []
}
