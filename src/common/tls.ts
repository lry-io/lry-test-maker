export const TLS_KEY = `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAt6wYw3Pq5KNy/v5AOGK3k+VJmEV2x6xYGAIlV8pH6xWVYnnF
PNr7EbGiziYgyVZ40x1NvKfjSGMnqbmdlttEim1/oCrb5SoDsZRk1jDH0h0OJv2q
m8HutiwRQujb1tRi1aUumZlQVFmiThcm9HeVuEFueGJ2jgWsaAHK9DgsUN+lLaB0
OPpOXinkybEkC4FGshwAMpMCpQr67XFy6cI5KSK//OYM+sqJUt9cSGmzLmvJ1B71
1g097/tAOOXcGlfA45VR29A+Ffmw5DVBMqcW2OXYaT+VwcYrnISF5sNOYt2hNiJ/
i4wGWab89nQXKgdK3YNpDTjMxEeiBckOwWW3jwIDAQABAoIBACDVB5wwpWV3fj/s
rQgnRU+PpgctK51nxluMTqaRxydTtV9kCwjk0AqKqWKWWDgFWBXM4C+AB1XSO63Q
O0ZTLb5c8npsDuC5EO32a8wJ7j416Wi44X10PjqcR/2g6GNPlddA7sKQMFbTqvqw
kssScVX9hETKvQyIIRh6cZAUVn+iHQX9PoFDpEYcGALHkdbO8IVaODOfI3F9h2+o
INUS2UEd6riX2HKyE6JNJX5nqba9vBOgnYnlgmgMs43iOej5pw/RcP+wzWuf7qwy
ZvztFiSqctVZ1UHDyf8bjtRVzxms4nhZvrvJl99hQq6Hrc5iH84Uli6B8OHe6eKp
EJhfAikCgYEA6WNpIb3EPBRkslp9kUFODvNQjpf1AVyS4tGXj/bcqfEwLt90RMQw
/m6wVXVbFMkGYHgOnA9srbXuZT0or4Z9mYM1PQdu2c2nqdLRc51R3DEyTShEKKlJ
JFC1Y4c5r0N/d1E1oO7IkJh8qW1iBzoctbwO+56baPCmFYjJifmOk2sCgYEAyXeb
xDu/tR2C5Ph8p+msGP4g388Ik8+gWQcqnsPgorCRkWNQ2YfFCxc2s4h+WNpn0b2C
l00EmMJA32J+Tzsekph1kPKsDwb5aPERkZdt2uzqpQWpDSw4VHsV3yLLx+kFe9XX
q8Wvj1geSMnmBcCsUjbiJGvhbfhkIJGmT9hCmW0CgYEAhV92GP4lQmA1G2dzEqtL
PGf3xUU5kkmg91XnUzIz8DoQ2vh/rTyg8wW2Z1/laP9oUYDKeIyoAq1RSzGkKoql
BDFjHzk8M+O+yOHdKEDjIv5KTKWSLdpwHfyxa+s/H9Vm5nzWNN3AaEF8HCaSOqcO
+Rw7Q7fkABhmETBWXC7tN3kCgYAvM+n4CTO6gKaZdSrnwJUE6g3TwlM1JpFgKpCu
0CACnjINCWL5BJmDJrY3TESmFs36U1eUkYQBMoo2jIIhmC1qlXP+OQ1dG/o1vz2p
ifxALgAwF+pq3rB4Arq2JBXwycTyJcE5SsD+adv2m+9b66+0N422kNwpgugaxWVd
EcNQ5QKBgBfot14lIV0w0CJDaJIXTcRTptQ7SbZ2JjZ6sQ+i6TfakBj2kLQUpvkj
bR3jv6lyvzbmptz6Kaf8fSZFC1KstPtfGKauLp8TisTcj9GVOAXHZLEiiolxyskC
Srr1fRQIOyVVAmEU4FhrdGkWojNTXlQzcbvGpO2/2A9X4F8HAD70
-----END RSA PRIVATE KEY-----`

export const TLS_CERT = `-----BEGIN CERTIFICATE-----
MIIDPzCCAicCFHM3Zdx8jSuKkCgHfPBbBV96HQr+MA0GCSqGSIb3DQEBCwUAMFwx
CzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMRcwFQYDVQQKDA5MUlkg
VGVzdCBNYWtlcjEfMB0GA1UEAwwWaHR0cHM6Ly9sb2NhbGhvc3Q6ODg4NTAeFw0y
MjAxMTEwNDE0MzRaFw00OTA1MjgwNDE0MzRaMFwxCzAJBgNVBAYTAkFVMRMwEQYD
VQQIDApTb21lLVN0YXRlMRcwFQYDVQQKDA5MUlkgVGVzdCBNYWtlcjEfMB0GA1UE
AwwWaHR0cHM6Ly9sb2NhbGhvc3Q6ODg4NTCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBALesGMNz6uSjcv7+QDhit5PlSZhFdsesWBgCJVfKR+sVlWJ5xTza
+xGxos4mIMlWeNMdTbyn40hjJ6m5nZbbRIptf6Aq2+UqA7GUZNYwx9IdDib9qpvB
7rYsEULo29bUYtWlLpmZUFRZok4XJvR3lbhBbnhido4FrGgByvQ4LFDfpS2gdDj6
Tl4p5MmxJAuBRrIcADKTAqUK+u1xcunCOSkiv/zmDPrKiVLfXEhpsy5rydQe9dYN
Pe/7QDjl3BpXwOOVUdvQPhX5sOQ1QTKnFtjl2Gk/lcHGK5yEhebDTmLdoTYif4uM
Blmm/PZ0FyoHSt2DaQ04zMRHogXJDsFlt48CAwEAATANBgkqhkiG9w0BAQsFAAOC
AQEArTHIsUrf7RrAK4hlwcgnw8K3WGunOg08iT2ouMduSIcxgHnXcJUxuLwsLugp
qLaSvDcFfQoAG9Oikqu2R8USjMjlv3CyYmoDO+GLfZIAulgoozWDAqnilOsGZ5yi
FrU81Xv27qjIcomaMV+bjRYyMPkreZDLkqjLCINFFPaE8GATrugME0Vrx4kncc8J
MVhdNje136xraAfxnmOjM662v2aqMLpWD8twqeLUQuBtp7eeYUZa4FY6B5dulwQV
v9U502JReWpT5wBE6jLmbL67PCkfbuaunIV87FwyDkMpyBaIDhk05UvJlzRifEou
GJI66l9N/LInf99/BfbTJ8Am2Q==
-----END CERTIFICATE-----`