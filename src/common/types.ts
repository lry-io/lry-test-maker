
export enum Action {
	Click = "Click",
	Write = "Write",
	Press = "Press",
	Scroll = "Scroll",
	Verify = "Verify"
}

export enum SelectorType {
	DOCUMENT = "DOCUMENT",
	BODY = "BODY",
	Text = "Text",
	Name = "Name",
	Placeholder = "Placeholder",
	Value = "Value",
	ID = "ID",
	LabelFor = "Label",
	NestedInLabel = "NestedInLabel",
	AriaLabel = "AriaLabel",
	DataTestID = "DataTestID",
	CantIdentify = "CantIdentify"
}

export interface TestStep {
	browserNumber: number
	action: Action
	selectorType: SelectorType
	target: string
	time: number
	data: string
}

export enum Command {
	START_RECORDING = "START_RECORDING",
	STOP_RECORDING = "STOP_RECORDING",
	START_VERIFICATION = "START_VERIFICATION",
	STOP_VERIFICATION = "STOP_VERIFICATION",
	PRINT = "PRINT",
	REPLAY = "REPLAY",
	DEMO_REPLAY = "DEMO_REPLAY"
}

export enum RecordingState {
	SETUP = "SETUP",
	RECORDING = "RECORDING",
	FINISHED = "FINISHED"
}

export interface CurrentState {
	state: RecordingState,
	testDefinition: TestDefinition
}

export function isCurrentState(obj: Object | undefined): obj is CurrentState {
	return !!obj && obj.hasOwnProperty('state') && obj.hasOwnProperty('testDefinition')
}


export interface TestDefinition {
	urls: string[],
	testSteps: TestStep[]
}

export const sessionStorageKey = 'lry-test-maker-test-steps'

export const server_and_proxy_host = 'localhost'
export const server_and_proxy_port = 8884

export const server_and_proxy_address = `${server_and_proxy_host}:${server_and_proxy_port}`