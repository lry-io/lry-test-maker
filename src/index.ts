import * as open from "open"
import { DefinitionServer } from "./definition_server/definition_server"
import * as minimist from "minimist"
import { server_and_proxy_address } from "./common/types"

const parsedArgs = minimist(process.argv)

async function runProgram() {
	if (parsedArgs.p) {
		const controller = new DefinitionServer('./.lrytest.script')
		await controller.loadFromFile()
		await controller.demoReplay()
	}
	else {
		const controller = new DefinitionServer('./.lrytest.script')
		await controller.init()
		open(`http://${server_and_proxy_address}/lry-test-maker/web/index.html`)
	}
}

runProgram()

