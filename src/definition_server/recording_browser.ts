import { Builder, ThenableWebDriver } from "selenium-webdriver"
import * as chrome from 'selenium-webdriver/chrome';
import { server_and_proxy_address, sessionStorageKey, TestStep } from "../common/types"
let end = false

let syncTimer: NodeJS.Timer
let errorCount = 0
const errorThreshold = 15

export class RecordingBrowser {

	public browser?: ThenableWebDriver

	public testSteps: TestStep[]

	constructor(public browserNumber: number, public testPage: string = '', public headless = false, private caFingerprint: string) {
		testPage && this.setTestPage(testPage)
		this.testSteps = []
	}

	setTestPage(url: string) {
		if (url.indexOf('//localhost') != -1) {
			//TODO: make this help website
			throw new Error("can't record from localhost, please use your IP address or add a hosts file entry - see https://lry.io/docs/localhost.html")
		}
		this.testPage = url
	}

	async start() {
		if (!this.testPage) {
			throw new Error("test url not set")
		}
		let option = new chrome.Options().addArguments(`--proxy-server=${server_and_proxy_address}`, `--ignore-certificate-errors-spki-list=${this.caFingerprint}`)
		if (this.headless) {
			option.addArguments(`--proxy-server=${server_and_proxy_address}`, 'headless', 'disable-gpu', `--ignore-certificate-errors-spki-list=${this.caFingerprint}`)
		}

		this.browser = new Builder()
			.forBrowser('chrome')
			.setChromeOptions(option)
			.build()

		await this.browser.get(this.testPage)

		const self = this
		syncTimer = setInterval(async () => {
			await self.synchroniseActionsAndCommands()
			if (end) {
				self.browser && self.browser.close()
				clearInterval(syncTimer)
			}
		}, 50)
	}

	async synchroniseActionsAndCommands() {
		try {
			if (this.browser) {
				const testStepString: string = await this.browser.executeScript(`window.lryTestMaker.setBrowserNumber(${this.browserNumber}); const testStepString = window.sessionStorage.getItem("${sessionStorageKey}"); return testStepString`)
				if (testStepString) {
					const steps = JSON.parse(testStepString)
					this.testSteps = steps
				}
			}
		}
		catch (e) {
			console.log('browser sync error: ', e)
			errorCount++
			if (errorCount > errorThreshold) {
				console.log(`Browser ${this.browserNumber} - too many errors trying to synchronise, closing.`)
				clearInterval(syncTimer)
			}
		}
	}

	async clearSteps() {
		this.testSteps = []
		if (this.browser) {
			await this.browser.executeScript(`window.sessionStorage.setItem("${sessionStorageKey}", []);`)
		}
	}

	async startRecording() {
		if (!this.browser) {
			throw new Error(`browser number ${this.browserNumber} does not exist`)
		}
		this.browser.executeScript(`window.lryTestMaker.startRecording()`)
	}
	async stopRecording() {
		if (!this.browser) {
			throw new Error(`browser number ${this.browserNumber} does not exist`)
		}
		this.browser.executeScript(`window.lryTestMaker.stopRecording()`)
	}
	async startVerifying() {
		if (!this.browser) {
			throw new Error(`browser number ${this.browserNumber} does not exist`)
		}
		this.browser.executeScript(`window.lryTestMaker.startVerifying()`)
	}
	async stopVerifying() {
		if (!this.browser) {
			throw new Error(`browser number ${this.browserNumber} does not exist`)
		}
		this.browser.executeScript(`window.lryTestMaker.stopVerifying()`)
	}
	async stop() {
		clearInterval(syncTimer)
		if (this.browser) {
			try {
				await this.browser.close()
			}
			catch (e) {
				console.log(`error trying to close browser ${this.browserNumber}: `, e)
			}
		}

	}
}

