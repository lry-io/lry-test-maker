
import * as  mockttp from 'mockttp'
import { isCurrentState, server_and_proxy_address, server_and_proxy_port } from '../common/types'
import { fileLibrary } from '../generated/fileLibrary'
import { DefinitionServer } from './definition_server'
import { readFile } from 'fs/promises'

const browserConnectorScript = '/lry-test-maker/browser-connector.js'
const testApiPath = '/lry-test-maker/api';
const testApiMatch = /\/lry-test-maker\/api.*/;
const webContentMatch = /\/lry-test-maker\/web.*/;

let testRecordingServer: DefinitionServer
let server: mockttp.Mockttp | undefined

export async function startServer(defServer: DefinitionServer) {
	testRecordingServer = defServer
	// Create a proxy server with a self-signed HTTPS CA certificate:
	const https = await mockttp.generateCACertificate();
	const server = mockttp.getLocal({ https });

	// Serve the connector script
	server.forGet(browserConnectorScript).thenFromFile(200, `dist/${browserConnectorScript}`);

	// Route API requests
	server.forGet(testApiMatch).thenCallback((request) => {
		return routeApiCalls(request)
	});

	// Route API requests
	server.forPost(testApiMatch).thenCallback((request) => {
		return routeApiCalls(request)
	});

	server.forGet(webContentMatch).thenCallback((request) => {
		return serveWebContent(request)
	})

	server.forUnmatchedRequest().thenPassThrough({
		beforeResponse: async (res) => {
			const contentType = res.headers['content-type']
			if (contentType && contentType.indexOf('text/html') >= 0) {
				const text = await res.body.getText()
				if (text) {
					const headers = res.headers
					const body = injectScriptTag(text);
					headers['content-length'] = body.length + ''
					headers['content-encoding'] = ''
					return {
						headers,
						body
					}
				}
			}
			// return {}
		}
	})

	await server.start(server_and_proxy_port);

	// Print out the server details:
	const caFingerprint = mockttp.generateSPKIFingerprint(https.cert)
	console.log(`Server running on port ${server.port}`);
	console.log(`CA cert fingerprint ${caFingerprint}`);

	return caFingerprint
}

function injectScriptTag(markup: string) {
	return markup.replace("<head>", `<head><script src="http://${server_and_proxy_address}${browserConnectorScript}"></script>`)
}

export function stopServer() {
	if (server) {
		server.stop()
	}
}

async function routeApiCalls(req: mockttp.CompletedRequest): Promise<mockttp.requestHandlers.CallbackResponseResult> {

	const url = new URL(req.url, `http://${req.headers.host}`);
	const path = url.pathname.slice(testApiPath.length)
	try {
		let success = true
		if (path === '/currentState') {
			if (req.method === "POST") {
				const jsonRequestBody = await req.body.getJson()
				if (isCurrentState(jsonRequestBody)) {
					testRecordingServer.setCurrentState(jsonRequestBody)
				}
			}
		}
		else if (path === '/loadStepsFromFile') {
			await testRecordingServer.loadFromFile()
		}
		else if (path === '/saveStepsToFile') {
			await testRecordingServer.saveToFile()
		}
		else if (path === '/clearTest') {
			await testRecordingServer.clearTest()
		}
		else if (path === '/startTestRecording') {
			testRecordingServer.startTestRecording()
		}
		else if (path === '/pauseRecording') {
			testRecordingServer.pauseTestRecording()
		}
		else if (path === '/resumeRecording') {
			testRecordingServer.resumeTestRecording()
		}
		else if (path === '/endRecording') {
			testRecordingServer.endTest()
		}
		else if (path === '/addBrowser') {
			await testRecordingServer.addBrowser();
		}
		else if (path === '/demoReplay') {
			testRecordingServer.demoReplay()
		}
		else if (path === '/fastReplay') {
			testRecordingServer.fastReplay()
		}
		else if (path === '/error404') {
			success = false
			return {
				status: 500,
				json: { error: 'unknown command' }
			}
		}
		else if (path === '/error500') {
			success = false
			return {
				status: 500,
				json: { error: 'api method not found' }
			}
		}
		else {
			success = false
			return {
				status: 500,
				json: { error: 'api method not found' }
			}
		}

		if (success) {
			return {
				status: 200,
				json: testRecordingServer.getCurrentState()
			}
		}
		else {
			return {
				status: 500,
				json: { error: 'api method not found' }
			}
		}
	}
	catch (e: any) {
		return {
			status: 500,
			json: { error: e.message }
		}
	}
}


async function serveWebContent(req: mockttp.CompletedRequest): Promise<mockttp.requestHandlers.CallbackResponseResult> {

	const url = new URL(req.url, `http://${req.headers.host}`)
	let status = 200
	let contentType = 'text/html'
	let contentLength = '0'
	let error: string = ''
	let data: Buffer | undefined = undefined
	if (url.pathname.endsWith('.svg')) {
		contentType = 'image/svg+xml'
	}
	else if (url.pathname.endsWith('.ico')) {
		contentType = 'image/x-icon'
	}
	else if (url.pathname.endsWith('.html')) {
		contentType = 'text/html'
	}
	else if (url.pathname.endsWith('.js')) {
		contentType = 'application/javascript'
	}
	else if (url.pathname.endsWith('.css')) {
		contentType = 'text/css'
	}

	if (process.env.NODE_ENV === "production") {
		if (!fileLibrary[url.pathname]) {
			status = 404
			error = 'no file library content at path'
		}
		else {
			data = Buffer.from(fileLibrary[url.pathname], 'base64')
			contentLength = `${data.length}`
		}
	}
	else {
		try {
			data = await readFile(`dist${url.pathname}`)
			contentLength = `${data.length}`
		}
		catch (e) {
			status = 404
			error = 'not found'
		}
	}

	return {
		status,
		headers: {
			'content-type': contentType,
			'content-length': contentLength
		},
		body: data || error
	}

}