import { Command, CurrentState, RecordingState, TestDefinition, TestStep } from "../common/types"
import { RecordingBrowser } from "./recording_browser"
import { playTest } from "./test_player"
import { startServer, stopServer } from "./http_server_and_proxy"
import * as fs from 'fs'

export class DefinitionServer {

	recordingBrowsers: RecordingBrowser[]

	headless: boolean

	recordingState: RecordingState

	caFingerprint: string | undefined

	constructor(private saveFileLocation: string) {
		this.headless = false
		this.recordingState = RecordingState.SETUP
		this.recordingBrowsers = []
	}

	async init() {
		console.log("Automation test controller started")
		this.caFingerprint = await startServer(this)
		this.loadFromFile()
	}

	getCurrentState(): CurrentState {
		return {
			state: this.recordingState,
			testDefinition: this.getTestDefinition()
		}
	}

	setCurrentState(newState: CurrentState) {
		// TODO: Currently only updates the browser urls:
		this.recordingBrowsers.map((rb, i) => rb.setTestPage(newState.testDefinition.urls[i]))
	}

	setIsHeadless(isHeadless: boolean) {
		this.headless = true
		this.recordingBrowsers.map(rb => rb.headless = isHeadless)
	}

	addBrowser() {
		this.recordingBrowsers.push(new RecordingBrowser(this.recordingBrowsers.length, undefined, this.headless, this.caFingerprint || ''))
	}

	async startTestRecording() {
		this.recordingState = RecordingState.RECORDING
		await this.recordingBrowsers.map(async rb => await rb.start())
		await this.recordingBrowsers.map(async rb => await rb.startRecording())
	}

	async pauseTestRecording() {
		await Promise.all(this.recordingBrowsers.map(rb => rb.stopRecording()))
	}

	async resumeTestRecording() {
		await Promise.all(this.recordingBrowsers.map(rb => rb.startRecording()))
	}

	async endTest() {
		this.recordingState = RecordingState.FINISHED
		await this.closeBrowsers()
	}

	async loadFromFile() {
		try {
			if (!fs.existsSync(this.saveFileLocation)) {
				console.log(`no save file found at ${this.saveFileLocation}, a new config will be created when you save`)
			}
			else {
				const fileContents = fs.readFileSync(this.saveFileLocation)
				await this.clearTest()
				const testDefinition: TestDefinition = JSON.parse(fileContents.toString())
				testDefinition.urls.map((url, i) => this.recordingBrowsers.push(new RecordingBrowser(i, url, this.headless, this.caFingerprint || '')))
				testDefinition.testSteps.map(step => this.recordingBrowsers[step.browserNumber].testSteps.push(step))
				console.log(`test definition loaded from file - ${this.saveFileLocation} - with ${testDefinition.urls.length} browser(s) and ${testDefinition.testSteps.length} steps `)
			}
		}
		catch (e) {
			console.log(`error loading saved config file ${this.saveFileLocation}: `, e)
		}
	}

	saveToFile() {
		const testDefinition = this.getTestDefinition()
		fs.writeFileSync(this.saveFileLocation, JSON.stringify(testDefinition, null, 2))
		console.log(`test definition saved to file - ${this.saveFileLocation} - with ${testDefinition.urls.length} browser(s) and ${testDefinition.testSteps.length} steps `)
	}

	async stop() {
		stopServer()
	}

	/**
	 * Converts the test steps from each browser into a single serial
	 * list of test steps in timestamp order
	 */
	private getSerialisedTestSteps() {
		const stepLists = this.recordingBrowsers.map(rb => rb.testSteps)

		const serialisedActionList: TestStep[] = []
		for (let i = 0; i < stepLists.length; i++) {
			serialisedActionList.push(...stepLists[i])
		}

		return serialisedActionList.sort((a, b) => a.time - b.time)
	}

	private getTestDefinition(): TestDefinition {
		const urls = this.recordingBrowsers.map(rb => rb.testPage)
		return { urls, testSteps: this.getSerialisedTestSteps() }
	}

	async clearTest() {
		await this.endTest()
		this.recordingBrowsers = []
		console.log("test cleared")
	}

	async closeBrowsers() {
		await Promise.all(this.recordingBrowsers.map(rb => rb.stop()))
	}

	startVerifying() {
		this.recordingBrowsers.map(rb => rb.startVerifying())
		console.log("verifying started")
	}

	stopVerifying() {
		this.recordingBrowsers.map(rb => rb.startVerifying())
		console.log("verifying stopped")
	}

	fastReplay() {
		console.log("starting fast playback")
		playTest(this.getTestDefinition(), false)
		console.log("fast playback complete")
	}

	async demoReplay() {
		console.log("starting demo playback")
		await playTest(this.getTestDefinition(), true)
		console.log("demo playback complete")
	}
}
