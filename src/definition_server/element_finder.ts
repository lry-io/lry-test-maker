import { ThenableWebDriver, WebElement } from "selenium-webdriver";


/**
 * Checks if the element is in screen view.
 * We only care about the element's position in Y pixel.
 */
export async function isElementInScreenView(element: WebElement, driver: ThenableWebDriver, viewportHeight: number) {
	const elementHeight = (await element.getSize()).height;
	const elementLocationY = (await element.getLocation()).y;

	const pageYOffset = (await driver.executeScript("return window.pageYOffset;")) as number;

	// element's relative position to current viewport's top-left corner based on pageYOffset
	const elementPositionY = elementLocationY - pageYOffset;

	return (elementLocationY + elementHeight <= 0) ||
		((elementPositionY > 85) && (elementPositionY + elementHeight <= viewportHeight));
}