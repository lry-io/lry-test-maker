
import { Builder, By, Capabilities, ThenableWebDriver, WebElement } from "selenium-webdriver"
import { Action, server_and_proxy_address, SelectorType, TestStep } from "../common/types"
import { TestDefinition } from "../common/types"
import * as chrome from 'selenium-webdriver/chrome';
import { readFileSync } from "fs";

export async function playTest(testDefinition: TestDefinition, realtime: boolean = false) {
	playTestWithSelenium(testDefinition, realtime)
}

export async function playTestWithBrowserConnector(testDefinition: TestDefinition, realtime: boolean = false) {
	try {
		const browserCount = testDefinition.urls.length
		const browsers: ThenableWebDriver[] = []
		for (let i = 0; i < browserCount; i++) {
			let option = new chrome.Options().addArguments(`--proxy-server=${server_and_proxy_address}`)

			browsers.push(new Builder()
				.forBrowser('chrome')
				.setChromeOptions(option)
				.build())

			await browsers[i].get(testDefinition.urls[i])
		}
		console.log("playback browser pages loaded")
		for (let i = 0; i < testDefinition.testSteps.length; i++) {
			const testStep = testDefinition.testSteps[i]
			const browser = browsers[testStep.browserNumber]
			try {
				const result = await browser.executeScript(`window.lryTestMaker.playTestStep(${JSON.stringify(testStep)})`);
				console.log(JSON.stringify(result))
			}
			catch (e) {
				console.log("unexpected error running test: ", e)
			}
		}
	}
	catch (e) {
		console.log("Unexpected failure running the test definition: ", e)
	}
}

export async function playTestWithSelenium(testDefinition: TestDefinition, realtime: boolean = false) {
	try {
		const browserCount = testDefinition.urls.length
		const browsers: ThenableWebDriver[] = []
		for (let i = 0; i < browserCount; i++) {
			browsers.push(new Builder()
				.withCapabilities(Capabilities.chrome())
				.build())
			await browsers[i].get(testDefinition.urls[i])
			console.log(`playback browser page loaded: ${testDefinition.urls[i]}`)
		}

		let success = true
		for (let i = 0; i < testDefinition.testSteps.length; i++) {
			const testStep = testDefinition.testSteps[i]
			const browser = browsers[testStep.browserNumber]
			try {
				let elem: WebElement | undefined
				if (testStep.action === Action.Scroll) {
					if (testStep.selectorType === SelectorType.DOCUMENT) {
						await browser.executeScript(`window.scrollBy(${testStep.data})`)
					}
					else if (testStep.selectorType === SelectorType.BODY) {
						await browser.executeScript(`document.body.scrollBy(${testStep.data})`)
					}
					else {
						await browser.executeScript(`window.lryTestMaker.playTestStep(${JSON.stringify(testStep)})`)
					}
				}
				else {
					if (testStep.selectorType === SelectorType.Text) {
						elem = await browser.findElement(By.xpath(`//*[text()='${testStep.target}']`))
					}
					else if (testStep.selectorType === SelectorType.LabelFor) {
						try {
							const label = await browser.findElement(By.xpath(`//label[text()='${testStep.target}']`))
							const forElemId = await label.getAttribute('for')
							elem = browser.findElement(By.id(forElemId))
						}
						catch (e) {
							//catch and ignore element not found
						}
					}
					else if (testStep.selectorType === SelectorType.NestedInLabel) {
						try {
							elem = await browser.findElement(By.xpath(`//*[text()='${testStep.target}']`))
						}
						catch (e) {
							//catch and ignore element not found
						}
					}
					else if (testStep.selectorType === SelectorType.AriaLabel) {
						try {
							elem = await browser.findElement(By.xpath(`//*[@aria-label='${testStep.target}']`))
						}
						catch (e) {
							//catch and ignore element not found
						}
					}
					else if (testStep.selectorType === SelectorType.Placeholder) {
						elem = await browser.findElement(By.xpath(`//*[@placeholder='${testStep.target}']`))
					}
					else if (testStep.selectorType === SelectorType.Name) {
						elem = await browser.findElement(By.xpath(`//*[@name='${testStep.target}']`))
					}
					else if (testStep.selectorType === SelectorType.ID) {
						elem = await browser.findElement(By.id(testStep.target))
					}
					if (elem) {
						if (testStep.action === Action.Click) {
							await elem.click()
						}
						else if (testStep.action === Action.Write) {
							await elem.sendKeys(testStep.data)
						}
						else if (testStep.action === Action.Verify) {
							if (!elem) {
								console.log(`Verify failed - Can't find element: ${testStep.selectorType} - ${testStep.target}`)
								success = false
								break
							}
						}
					}
					else {
						console.log(`Failure - Can't find element: ${testStep.action} - ${testStep.target}`)
						success = false
						break
					}
					if (realtime && testDefinition.testSteps[i + 1]) {
						await sleep(testDefinition.testSteps[i + 1].time - testStep.time)
					}
					console.log(`browser[${testStep.browserNumber}]: ${testStep.action} - ${testStep.target}`)
				}
			}
			catch (e) {
				console.log(`Failure: ${testStep.action} - ${testStep.target}`, e)
				success = false
				break
			}
		}
		await sleep(2000)
		for (let i = 0; i < browsers.length; i++) {
			browsers[i].close()
		}

		if (success) {
			console.log(`Test run successful with [${testDefinition.testSteps.length}] steps`)
		}
		else {
			console.log(`Test failed`)
		}
	}
	catch (e) {
		console.log(e)
	}
}

async function findElemWithSelenium(testStep: TestStep, browser: ThenableWebDriver) {
	let elem: WebElement | undefined
	if (testStep.selectorType === SelectorType.Text) {
		elem = await browser.findElement(By.xpath(`//*[text()='${testStep.target}']`))
	}
	else if (testStep.selectorType === SelectorType.LabelFor) {
		try {
			elem = await browser.findElement(By.xpath(`//label[text()='${testStep.target}']`))
		}
		catch (e) {
			//catch and ignore element not found
		}
	}
	else if (testStep.selectorType === SelectorType.NestedInLabel) {
		try {
			elem = await browser.findElement(By.xpath(`//*[text()='${testStep.target}']`))
		}
		catch (e) {
			//catch and ignore element not found
		}
	}
	else if (testStep.selectorType === SelectorType.AriaLabel) {
		try {
			elem = await browser.findElement(By.xpath(`//*[@aria-label='${testStep.target}']`))
		}
		catch (e) {
			//catch and ignore element not found
		}
	}
	else if (testStep.selectorType === SelectorType.Placeholder) {
		elem = await browser.findElement(By.xpath(`//*[@placeholder='${testStep.target}']`))
	}
	else if (testStep.selectorType === SelectorType.ID) {
		elem = await browser.findElement(By.id(testStep.target))
	}
	return elem
}

const sleep = (milliseconds: number) => {
	return new Promise(resolve => setTimeout(resolve, milliseconds))
}