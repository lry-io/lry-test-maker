import { Expect, Test, TestFixture, Timeout } from "alsatian";
import { Builder, By, ThenableWebDriver } from "selenium-webdriver";
import { Action, SelectorType, server_and_proxy_address, server_and_proxy_host } from "../common/types";
import { DefinitionServer } from "../definition_server/definition_server";
import * as chrome from 'selenium-webdriver/chrome';
import { writeFileSync } from "fs";

let headless = true

// the hostname "ltmtestsite" is also used in gitlab cicd as 
// the web content container name
// so that the tests running in gitlab will load test content from there.
let test_content_url = "http://ltmtestsite/"

console.log(process.env)
if (process.env.LTMCONTENTSITE) {
	test_content_url = process.env.LTMCONTENTSITE
}

if (process.env.LTMHEADFUL) {
	headless = false
}

async function takeScreenshot(browser: ThenableWebDriver, name: string) {
	let image = await browser.takeScreenshot()
	let save_location = `/tmp/ltmscreenshots/${name}`
	writeFileSync(save_location, image, 'base64')
	console.log(`screenshot ${save_location} taken`)
}

function getGSID(browser: ThenableWebDriver, id: string) {
	return browser.findElement(By.xpath(`//*[@data-gs-internal-test-id="${id}"]`))
}
@TestFixture("recording tests")
export class RecordingTests {

	@Test("record a click")
	@Timeout(60000)
	public async recordClick() {
		let testRecordingServer;
		let testRecordingInterface;

		try {
			testRecordingServer = new DefinitionServer('non-existent-test-save-file')
			testRecordingServer.init()
			testRecordingServer.setIsHeadless(headless)
			const options = new chrome.Options().addArguments('headless', 'disable-gpu',)
			console.log("creating browser to run the web ui")
			testRecordingInterface = new Builder()
				.forBrowser('chrome')
				.setChromeOptions(options)
				.build()

			console.log("loading the test ui")
			await testRecordingInterface.get(`http://${server_and_proxy_address}/lry-test-maker/web/index.html`)
			takeScreenshot(testRecordingInterface, "page-loaded.png")
			await testRecordingInterface.findElement(By.xpath('//*[text()="Add a browser"]')).click()
			takeScreenshot(testRecordingInterface, "add-browser-clicked.png")
			await testRecordingInterface.findElement(By.xpath('//*[@placeholder="Browser 1 URL"]')).sendKeys(test_content_url)
			takeScreenshot(testRecordingInterface, "url-entered.png")
			await testRecordingInterface.findElement(By.xpath('//*[text()="Start Recording"]')).click()
			const rb = testRecordingServer.recordingBrowsers[0]
			await sleep(2000)
			if (rb && rb.browser) {
				await getGSID(rb.browser, 'test1Button').click()
			}
			else {
				throw new Error("starting the recording browser failed")
			}
			await testRecordingInterface.findElement(By.xpath('//*[text()="Stop Recording"]')).click()
			const state = testRecordingServer.getCurrentState()
			const steps = state.testDefinition.testSteps
			Expect(steps[0]).toEqual(
				{
					"action": Action.Click,
					"browserNumber": 0,
					"data": "",
					"selectorType": SelectorType.Text,
					"target": "Hello world",
					"time": steps[0].time
				}
			)
		}
		catch (e) {
			if (testRecordingInterface) {
				takeScreenshot(testRecordingInterface, "test-failure.png")
			}
			console.log(e)
			throw e
		}
		finally {
			try {
				if (testRecordingInterface) {
					testRecordingInterface.close()
				}
			}
			catch (e) { }
			if (testRecordingServer) {
				testRecordingServer.stop()
			}
		}
	}
}

const sleep = (milliseconds: number) => {
	return new Promise(resolve => setTimeout(resolve, milliseconds))
}