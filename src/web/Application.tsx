import { ControllerComponent } from "./ControllerComponent";
import * as React from 'react';
import { useState, useEffect } from 'react';
import { Snackbar } from "@material-ui/core";
import { style } from "typestyle"

const applicationStyle = style({
	margin: "1rem"
})

export function Application() {
	const [error, setError] = useState<Error>()
	return (
		<div className={applicationStyle}>
			<ControllerComponent setError={setError} />
			<Snackbar
				open={!!error}
				message={error?.message}
				autoHideDuration={6000}
				onClose={() => setError(undefined)}
			/>
		</div>
	)
}