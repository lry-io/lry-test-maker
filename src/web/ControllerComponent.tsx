import * as React from 'react';
import { useState, useEffect } from 'react';
import { Button, ButtonGroup, LinearProgress, TextField } from '@material-ui/core';
import MouseIcon from '@material-ui/icons/MouseTwoTone';
import WriteIcon from '@material-ui/icons/Keyboard';
import ScrollIcon from '@material-ui/icons/OpenWith';
import Fab from '@material-ui/core/Fab';
import { apiGet, updateTestState } from './api_client';
import { Action, CurrentState, RecordingState, SelectorType, TestDefinition, TestStep } from '../common/types';
import { style } from 'typestyle';

interface Props {
	setError: (error: Error) => void
}

const testDefinitionStyle = style({
	display: "flex"
})

const emptyStepSpacerStyle = style({
	height: "3rem",
})

const stepStyle = style({
	display: "flex",
	height: "3rem",
	alignItems: "center",
	backgroundColor: "#FFEDBB",
	color: "#000",
	borderRadius: "1.5rem",
	marginBottom: "0.5rem",
	position: 'relative'
})

const stepLeftStyle = style({
	height: "3rem",
	width: "2.5rem",
	display: "flex",
	alignItems: "center",
	color: "#000",
	borderRadius: "1.5rem 0 0 1.5rem",
	marginRight: "0.5rem",
	paddingLeft: "1rem"
})

const testStepActionDefaultStyle = style({
	backgroundColor: '#220',
	color: "#fff"
})

const testStepActionTypeClickStyle = style({
	backgroundColor: '#F15946',
})

const testStepActionPressTypeStyle = style({
	backgroundColor: '#F9C22E',
})

const testStepActionTypeWriteStyle = style({
	backgroundColor: '#F9C22E',
})

const stepSetStyle = style({
	width: "20rem"
})

const stepContentStyle = style({
	display: "flex",
	width: "14.5rem",
	flexDirection: "column",
	marginLeft: "0.5rem"

})

const actionTargetStyle = style({
	width: "14.5rem",
	whiteSpace: "nowrap",
	overflow: "hidden",
	textOverflow: "ellipsis"
})

const stepDataStyle = style({
	width: "14.5rem",
	whiteSpace: "nowrap",
	overflow: "hidden",
	textOverflow: "ellipsis"
})

const selectorTagStyle = style({
	backgroundColor: 'black',
	height: '0.7rem',
	left: '3.5rem',
	position: 'absolute',
	top: 0,
	width: '0.7rem',
	borderRadius: '0 0 1rem 0'
})

const actionBackgroundColours = {
	[Action.Click]: "#FFC1B9",
	[Action.Press]: "#B9C4FF",
	[Action.Scroll]: "#E3B9FF",
	[Action.Verify]: "#D5FFB9",
	[Action.Write]: "#B9C4FF",
}


const selectorTagColours = {
	[SelectorType.DOCUMENT]: "#000000",
	[SelectorType.BODY]: "#FFFFFF",
	[SelectorType.AriaLabel]: "#731DD8",
	[SelectorType.DataTestID]: "#48A9A6",
	[SelectorType.ID]: "#E4DFDA",
	[SelectorType.LabelFor]: "#D4B483",
	[SelectorType.NestedInLabel]: "#75DDDD",
	[SelectorType.Placeholder]: "#09BC8A",
	[SelectorType.Name]: "#196C8A",
	[SelectorType.Text]: "#C1666B",
	[SelectorType.Value]: "#004346",
	[SelectorType.CantIdentify]: "#FF0000",
}

function getIconFromStepAction(action: Action) {
	if (action === Action.Click) {
		return (<MouseIcon />)
	}
	if (action === Action.Write) {
		return (<WriteIcon />)
	}
	if (action === Action.Scroll) {
		return (<ScrollIcon />)
	}
	else {
		return (<div></div>)
	}
}

// Palette:
// dark: 0C090D
// hot pink: E01A4F
// tangerine: F15946
// old yellow: F9C22E
// tangy powder blue: 53B3CB

const generateStepElem = (step: TestStep, stepNumber: number, browserNumber: number) => {
	if (step.browserNumber === browserNumber) {
		return (
			<div key={`list${browserNumber}step${stepNumber}`} className={`${stepStyle}`}>
				<div className={`${stepLeftStyle}`} style={{ backgroundColor: actionBackgroundColours[step.action] }}>{getIconFromStepAction(step.action)}</div>
				<div className={selectorTagStyle} style={{ backgroundColor: selectorTagColours[step.selectorType] }} />
				<div className={stepContentStyle}>
					<div className={actionTargetStyle}>{step.target}</div>
					<div className={stepDataStyle}>{step.data}</div>
				</div>
			</div>
		)
	}
	else {
		return (
			<div className={emptyStepSpacerStyle}></div>
		)
	}
}

export function ControllerComponent(props: Props) {
	const [currentState, setCurrentState] = useState<CurrentState>({ state: RecordingState.SETUP, testDefinition: { urls: [], testSteps: [] } })

	function get(path: string) {
		return async () => {
			await apiGet(path, props.setError, {}, setCurrentState)
		}
	}

	function setTestPage(url: string, i: number) {
		currentState.testDefinition.urls[i] = url
		setCurrentState({ ...currentState })
	}

	function updateState() {
		updateTestState(props.setError, currentState, setCurrentState)
	}

	useEffect(() => {
		get('/currentState')()
		return () => { }
	}, [])

	return (
		<div>
			<div>
				<ButtonGroup >
					<Button color="primary" variant="contained" onClick={get('/loadStepsFromFile')}>Load test</Button>
					<Button color="secondary" variant="contained" onClick={get('/saveStepsToFile')}>Save test</Button>
					<Button variant="contained" onClick={get('/clearTest')}>Clear test</Button>
				</ButtonGroup>
			</div>
			<div>
				<h1>Test Definition</h1>
			</div>
			<div>
				<ButtonGroup>
					<Button variant="contained" onClick={get('/addBrowser')}>Add a browser</Button>
					<Button color="primary" variant="contained" onClick={get('/startTestRecording')}>Start Recording</Button>
					<Button color="secondary" variant="contained" onClick={get('/endRecording')}>Stop Recording</Button>
					<Button variant="contained" onClick={get('/currentState')}>Refresh data</Button>
					<Button variant="contained" onClick={get('/demoReplay')}>Demo replay</Button>
					<Button variant="contained" onClick={get('/fastReplay')}>Fast replay</Button>
				</ButtonGroup>
			</div>
			<h2>Steps</h2>
			<div className={testDefinitionStyle}>
				{currentState.testDefinition.urls.map((url, i) => (
					<div key={`stepList${i}`} className={stepSetStyle}>
						<div style={{
							margin: "1rem 0"
						}}>
							<TextField focused value={url} onChange={(event) => setTestPage(event.target.value, i)} onBlur={updateState} placeholder={`Browser ${i + 1} URL`} variant="outlined" />
						</div>
						<div>
							{currentState.testDefinition.testSteps.map((step, j) => generateStepElem(step, j, i))}
						</div>
					</div>
				))}
			</div>

		</div >
	);
}