import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Application } from './Application'
import { normalize } from 'csstips'
import { cssRule } from 'typestyle'

normalize()

cssRule('html', {
	backgroundColor: "white"
})

ReactDOM.render(
	<div>
		<Application />
	</div>
	, document.getElementById("application-base"))