import { CurrentState, isCurrentState } from "../common/types"

const apiBase = '/lry-test-maker/api'

export async function apiGet(
	path: string,
	setError: (error: Error) => void,
	paramMap: { [index: string]: string },
	setCurrentState: (state: CurrentState) => void
): Promise<CurrentState | null> {
	let params
	params = Object.keys(paramMap).map(key => `${key}=${paramMap[key]}`).join('&')
	let response: Response
	response = await fetch(`${apiBase}${path}?${params}`)
	if (!response.ok) {
		setError(new Error(await response.text()))
	}
	if (response.body) {
		const respJson = await response.json()
		if (isCurrentState(respJson)) {
			setCurrentState(respJson)
		}
		else {
			setError(new Error('response was not a valid current state'))
		}
	}
	return null
}


export async function updateTestState(
	setError: (error: Error) => void,
	state: CurrentState,
	setCurrentState: (state: CurrentState) => void
): Promise<CurrentState | null> {
	let response: Response
	response = await fetch(`${apiBase}/currentState`, {
		headers: [['Content-Type', 'application/json']],
		method: "POST",
		body: JSON.stringify(state)
	})
	if (!response.ok) {
		setError(new Error(await response.text()))
	}
	if (response.body) {
		const respJson = await response.json()
		if (isCurrentState(respJson)) {
			setCurrentState(respJson)
		}
		else {
			setError(new Error('response was not a valid current state'))
		}
	}
	return null
}