import { TestSet, TestRunner } from "alsatian"
import { TapBark } from "tap-bark"
(async () => {
    // Setup the alsatian test runner
    let testSet = TestSet.create();
    const fileToTest = process.argv[2];
    console.log(`running tests in file: ${fileToTest}`)
    testSet.addTestsFromFiles(fileToTest);

    // Runs the tests
    const testRunner = new TestRunner();

    // setup the output
    testRunner.outputStream
        // this will use alsatian's default output if you remove this
        // you'll get TAP or you can add your favourite TAP reporter in it's place
        .pipe(TapBark.create().getPipeable())
        // pipe to the console
        .pipe(process.stdout);

    // run the test set
    testRunner.run(testSet)
        // this will be called after all tests have been run
        .then((results) => {
            console.log(results)
        })
        .catch((error: any) => {
            console.log(error)
        })
})()
    .catch(e => {
        console.error(e);
        process.exit(1);
    });
