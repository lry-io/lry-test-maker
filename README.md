# lry-test-maker

1. Download and run LRY TEST MAKER (TODO: link releases)
2. Put in the website you want to test
3. Do things in the website
4. Save the steps to a file
5. Replay the tests in your build system of choice to get full, fun and easy web testing.

# How to use it

Download the lry-test-maker executable for your OS and run it. It will open a browser page where you can record tests, run them and save the steps to a file.

# Licensing

FREE FOR GPLv3 Apps (and very affordable for commercial use!)

If you want to use LRY Test Maker to build commercial sites, applications, or in a continuous-integration system that's closed-source then you'll need to purchase a commercial license. [You can purchase a commercial license here](https://www.lry.io/purchasing). A commercial license grants you:

- Priority support on issues and features.
- Support with getting it running in your CICD pipelines.
- Ability to modify the source (forking) for your own purposes.

If you are creating an open source application under a license compatible with the GNU GPL license v3, you may use LRY TEST MAKER at no cost under the terms of the GPLv3. You can read more about this license [here](https://www.gnu.org/licenses/quick-guide-gplv3.en.html).

# Development

## Tooling:
- Node >= 14.15.3
- Yarn (instead of npm)

(if you use VSCode as your IDE there are already some run scripts configured too )

## Quick start to just try it out locally
- `yarn`
- `yarn build`
- `yarn run:application`

This will start the application on your computer which then opens a browser page where you can do everything from playing back existing tests, edit them and record new ones. See the help info in the web control page for more info on how to use it.

## The different parts of the app

src/definition_server - this is the node app that runs on your computer recording test steps and controlling test browsers. You can run and debug it like a normal node app.

src/definition_server/web - this is the UI for the application, the definition server running on your computer serves this content as well as the API that it uses, it's all local on your machine.

src/browser_connector - this compiles into a is a standalone script that is inserted into the website that you want to test.

## Running it for development

The most common way is to run `yarn watch` in a terminal window, then run the application inside your IDE. (If you use VSCode as your IDE there are run scripts already set up). The watch task will update the browser connector script and the web ui when you change them and then you can modify the application in your IDE. Note, when you do it this way there's no webpack hot reloading of the content so if you change the web ui you'll need to reload the browser to see the changes.

## Testing the application itself

The app has end-to-end tests for itself. The tests need to start the app, serve the test content site and then drive the application through setting up tests, interacting with the content and then checking that the steps worked and are saved to file successfully. 

To run the tests locally you need to:
1. add a hostname into your /etc/hosts: `127.0.0.1 ltmtestsite` (see Gotchas below)
2. start the test content site with: `yarn start:test-content`
3. run `yarn test:local`.

## Gotchas

1. Can't proxy localhost in Chromium:
This whole system relies on proxying the sites that are being tested. We proxy them so that we can insert a script into them to record the test steps. Chromium browser doesn't allow proxying of the hostname "localhost" for security reasons, so that's why we need to set a new hostname for localhost called ltmtestsite. It's just localhost with a different name.