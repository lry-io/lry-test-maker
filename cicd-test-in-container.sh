# This script is run in the test stage in gitlab-ci.
# 
# It is executed inside the build container (see lry-test-maker-build-container)
# runs the tests as a non root user (because chromium doesn't like running as root)
# and then as root, moves any error screenshots into the test-output folder
# so that it works in podman.

set -o
mkdir -p /tmp/ltmscreenshots
chmod 777 /tmp/ltmscreenshots
mkdir -p test-output
su ltmuser -c "yarn test:cicd";
RETVAL=$?
cp -R /tmp/ltmscreenshots/* test-output/
ls -lAR test-output
set -e
exit $RETVAL