
This work is dual-licensed under GPL-3.0 OR the LRY Test Maker Commercial License
You can choose between one of them if you use this work.

`SPDX-License-Identifier: GPL-3.0-or-later OR LRY Test Maker Commercial License`
