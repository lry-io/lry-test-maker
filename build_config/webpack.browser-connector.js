const path = require('path');

module.exports = {
	mode: "production",
	entry: './src/browser-connector/browser-connector.ts',
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				loader: "ts-loader",
				options: {
					configFile: "build_config/tsconfig.browser-connector.json"
				},
				exclude: /node_modules/,
			}
		],
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},
	output: {
		filename: 'browser-connector.js',
		path: path.resolve(__dirname, '../dist/lry-test-maker/'),
	},
	devtool: 'source-map'
};
