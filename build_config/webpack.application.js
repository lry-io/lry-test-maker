const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
	mode: "development",
	entry: './src/index.ts',
	target: 'node',
	externals: [nodeExternals()],
	module: {
		rules: [
			{
				test: /\.ts$/,
				loader: "ts-loader",
				options: {
					configFile: "build_config/tsconfig.application.json"
				},
				exclude: /node_modules/,
			}
		],
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},
	output: {
		filename: 'application.js',
		path: path.resolve(__dirname, '../dist/lry-test-maker/application'),
	}
};
