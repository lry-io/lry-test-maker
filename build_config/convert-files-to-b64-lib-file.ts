import * as fs from 'fs'

const outputDir = 'src/generated'
const outputLibFile = `${outputDir}/fileLibrary.ts`

const library: { [index: string]: string } = {}

function convert(baseDirPath: string, prefixToIgnore: string) {
	const files = fs.readdirSync(baseDirPath)
	files.map(file => {
		const path = `${baseDirPath}/${file}`
		if (!(fs.lstatSync(path)).isDirectory()) {
			library[path.slice(prefixToIgnore.length)] = fs.readFileSync(path, 'base64')
		}
		else {
			convert(path, prefixToIgnore)
		}
	})
}

convert('dist/lry-test-maker', 'dist')
if (!fs.existsSync(outputDir)) {
	fs.mkdirSync(outputDir)
}
fs.writeFileSync(outputLibFile, "export const fileLibrary: { [index: string]: string } = " + JSON.stringify(library, null, 2))


