const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

module.exports = {
	mode: "development",
	entry: './src/web/index.tsx',
	module: {
		rules: [
			{
				test: /\.svg/,
				type: 'asset/resource',
				loader: "file-loader"

			},
			{
				test: /\.ts(x?)$/,
				loader: "ts-loader",
				options: {
					configFile: "build_config/tsconfig.web-ui.json"
				},
				exclude: /node_modules/,
			}
		],
	},
	plugins: [
		new FaviconsWebpackPlugin('design-assets/gun-favicon.svg'),
		new HtmlWebpackPlugin({
			template: 'src/web/app.template.html'
		}),
	],
	resolve: {
		extensions: ['.ts', '.js', '.tsx'],
	},
	output: {
		filename: '[chunkhash].js',
		path: path.resolve(__dirname, '../dist/lry-test-maker/web'),
	},
	devServer: {
		hot: true,
		port: 9090
	},
	devtool: 'source-map'
};
